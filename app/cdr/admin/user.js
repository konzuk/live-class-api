var response = require(__base + 'app/config/response');
var db = require(__base + 'app/driver/mysql');

var _documents = function(cnn, lessons, callback){
    var totalLesson = lessons.length;
    var video_lesson_base_thumb_url = __server_host+'assets/web_assets/img/lession_thumb/';
    var lesson_base_pdf_url = __server_host+'assets/upload/1/lsd_file/';    
    var no = 0;                
    for (i = 0; i < totalLesson; i++) {
        if(lessons[i]['lsn_thumbnail']){
            lessons[i]['lsn_thumbnail_full_url'] = video_lesson_base_thumb_url+lessons[i]['lsn_thumbnail'];
        }else{
            var name = (lessons[i]['lsn_id'] % 3)+1;
            lessons[i]['lsn_thumbnail_full_url'] = video_lesson_base_thumb_url+lessons[i]['tch_subject_id'] +'-'+name+'.jpg';                                            
        }

        sql = `SELECT
            admin_lesson_doc.lsd_id,
            admin_lesson_doc.lsd_title,
            admin_lesson_doc.lsd_file
            FROM
            admin_lesson_doc
            WHERE
            admin_lesson_doc.lsd_lesson_id = `+ cnn.escape(lessons[i]['lsn_id']);
        cnn.query(sql, function(err, rows, fields) {
            if (err) throw err;
            if (rows.length > 0) {
                lessons[no]['admin_lesson_doc'] = rows;
            }else{
                lessons[no]['admin_lesson_doc'] = [];
            }
            no ++;
            if(totalLesson == no ){
                return callback(lessons);
            }
        });
        
        
    }
};

module.exports.subjects = function(req, res) {
    if (req.method === 'GET') {
            db.connect(function(cnn) {
                var sql = `SELECT
                    admin_teacher_info.tch_id,
                    admin_teacher_info.tch_firstname,
                    admin_teacher_info.tch_lastname,
                    admin_teacher_info.tch_phone,
                    admin_teacher_info.tch_email,
                    admin_teacher_info.tch_photo,
                    admin_subject_category.sub_name,
                    admin_subject_category.sub_description,
                    admin_teacher_info.tch_subject_id
                    FROM
                    admin_teacher_info
                    INNER JOIN admin_subject_category ON admin_teacher_info.tch_subject_id = admin_subject_category.sub_id
                    WHERE
                    admin_teacher_info.tch_deleted = 0`;
                cnn.query(sql, function(err, rows, fields) {
                    if (err) throw err;
                    if (rows.length > 0) {
                        var i;
                        for (i = 0; i < rows.length; i++) {
                            rows[i]['tch_photo_dir'] = 'https://e-schoolcambodia.com/assets/upload/1/tch_photo/';
                            rows[i]['tch_photo_src'] = 'https://e-schoolcambodia.com/assets/upload/1/tch_photo/'+rows[i]['tch_photo'];

                        }
                        response.json(req, res, {
                            'host': 'https://e-schoolcambodia.com/assets/upload/1/tch_photo/',
                            'data': rows
                        });
                        return;
                    } else {
                        response.json(req, res, {'data': [] } );
                    }
                });
            });
    } else {
        response.error(req, res, 405, "METHOD_DISALLOW");
    }
};

module.exports.subjected_videos = function(req, res) {
    if (req.method === 'GET') {
    	var subject_id = req.query.subject_id;
        var page = req.query.page == ''?1:req.query.page;
        if (subject_id) {            
            db.connect(function(cnn) {
                var sql = '';               
                var no_per_page = 20;
                var offset = 0;
                var limit = no_per_page;
                if(page > 1){
                    offset = (page * no_per_page) - no_per_page;
                }

                sql = `SELECT lsn_id,
                    admin_lesson.lsn_title,
                    admin_lesson.lsn_vimeo_url,
                    admin_lesson.isn_video_path_hd,
                    admin_lesson.isn_video_path_md,
                    admin_lesson.isn_video_path_sd,
                    admin_lesson.lsn_thumbnail,
                    admin_lesson.lsn_teacher_id,
                    admin_lesson.lsn_student_view,
                    admin_lesson.lsn_release_date,
                    admin_teacher_info.tch_firstname,
                    admin_teacher_info.tch_lastname,
                    admin_teacher_info.tch_subject_id
                    FROM
                    admin_lesson
                    INNER JOIN admin_teacher_info ON admin_lesson.lsn_teacher_id = admin_teacher_info.tch_id
                    WHERE
                    admin_lesson.lsn_active = 1 AND
                    admin_lesson.lsn_deleted = 0 AND
                    admin_teacher_info.tch_subject_id =  `+ cnn.escape(subject_id)+`
                    ORDER BY lsn_id DESC LIMIT `+limit+` OFFSET `+offset;                       
                
                cnn.query(sql, function(err, rows, fields) {
                    if (err) throw err;
                    if (rows.length > 0) {
                        _documents(cnn, rows,function(_lessons){
                            response.json(req, res, {
                                'host': __server_host + 'client/',
                                'data': _lessons
                            }); 
                            return; 
                        });
                        
                    } else {
                        response.json(req, res, {'data': [] } );
                    }
                });
            });
        } else {
            response.error(req, res, 406, "SUBJECT_REQUIRED");
        }
    } else {
        response.error(req, res, 405, "METHOD_DISALLOW");
    }
};

module.exports.video_lesson = function(req, res ) {
    if (req.method === 'GET') {
    	var lesson_id = req.query.lesson_id;
        var page = req.query.page == ''?1:req.query.page;
        if (lesson_id) {            
            db.connect(function(cnn) {
                var sql = '';               
                sql = `SELECT
                    admin_teacher_info.tch_subject_id
                    FROM
                    admin_lesson
                    INNER JOIN admin_teacher_info ON admin_lesson.lsn_teacher_id = admin_teacher_info.tch_id
                    WHERE
                    admin_lesson.lsn_active = 1 AND
                    admin_lesson.lsn_deleted = 0 AND
                    admin_lesson.lsn_id = `+ cnn.escape(lesson_id);
                
                
                cnn.query(sql, function(err, rows, fields) {
                    if (err) throw err;
                    if (rows.length > 0) {
                        db.connect(function(cnn) {
                            var subject = rows[0]['tch_subject_id'];
                            var sql = '';               
                            var no_per_page = 20;
                            var offset = 0;
                            var limit = no_per_page;
                            if(page > 1){
                                offset = (page * no_per_page) - no_per_page;
                            }

                            sql = `SELECT lsn_id,
                                    admin_lesson.lsn_title,
                                    admin_lesson.lsn_vimeo_url,
                                    admin_lesson.isn_video_path_hd,
                                    admin_lesson.isn_video_path_md,
                                    admin_lesson.isn_video_path_sd,
                                    admin_lesson.lsn_thumbnail,
                                    admin_lesson.lsn_teacher_id,
                                    admin_lesson.lsn_student_view,
                                    admin_lesson.lsn_description,
                                    admin_lesson.lsn_release_date,
                                    admin_teacher_info.tch_firstname,
                                    admin_teacher_info.tch_lastname,
                                    admin_teacher_info.tch_subject_id,
                                    admin_lesson_doc.lsd_id,
                                    admin_lesson_doc.lsd_title,
                                    admin_lesson_doc.lsd_file
                                    FROM
                                    admin_lesson
                                    INNER JOIN admin_teacher_info ON admin_lesson.lsn_teacher_id = admin_teacher_info.tch_id
                                    LEFT JOIN admin_lesson_doc ON admin_lesson.lsn_id = admin_lesson_doc.lsd_lesson_id
                                    WHERE
                                    admin_lesson.lsn_id != `+lesson_id+` AND
                                    admin_lesson.lsn_active = 1 AND
                                    admin_lesson.lsn_deleted = 0 AND
                                    admin_teacher_info.tch_subject_id = `+ cnn.escape(subject)+`
                                    ORDER BY lsn_id DESC LIMIT `+limit+` OFFSET `+offset;                           
                            
                            cnn.query(sql, function(err, rows, fields) {
                                if (err) throw err;
                                if (rows.length > 0) {
                                    userData = rows;
                                    console.log('Result : '+rows.length);
                                    var video_lesson_base_thumb_url = 'https://e-schoolcambodia.com/assets/web_assets/img/lession_thumb/';
                                    var lesson_base_pdf_url = 'https://e-schoolcambodia.com/assets/upload/1/lsd_file/';
                                    var i;
                                    for (i = 0; i < rows.length; i++) {
                                        rows[i]['lesson_pdf_url'] = lesson_base_pdf_url+rows[i]['lsd_file'];
                                        if(rows[i]['lsn_thumbnail']){
                                            rows[i]['lsn_thumbnail_full_url'] = video_lesson_base_thumb_url+rows[i]['lsn_thumbnail'];
                                        }else{
                                            var name = (rows[i]['lsn_id'] % 3)+1;
                                            rows[i]['lsn_thumbnail_full_url'] = video_lesson_base_thumb_url+rows[i]['tch_subject_id'] +'-'+name+'.jpg';                                            
                                        }                                        
                                    }

                                    response.json(req, res, {
                                        'host': __server_host + 'client/',
                                        'data': userData
                                    });
                                    return;
                                } else {
                                    response.json(req, res, {'data': [] } );
                                }
                            });
                        });
                    } else {
                        response.error(req,res,401,"RECORD_NOT_FOUND", true);
                    }
                });
            });
        } else {
            response.error(req, res, 406, "LESSON_REQUIRED");
        }
    } else {
        response.error(req, res, 405, "METHOD_DISALLOW");
    }
};

module.exports.search_video_lessons = function(req, res) {
    if (req.method === 'GET') {
        var title = req.query.title;
        var page = req.query.page == ''?1:req.query.page;
        title = title.replace(/^"(.*)"$/, '$1');
        
        if (title) {            
            db.connect(function(cnn) {
                var sql = '';
                var where = "admin_lesson.lsn_title LIKE '%"+title+"%'";
                var no_per_page = 20;
                var offset = 0;
                var limit = no_per_page;
                if(page > 1){
                    offset = (page * no_per_page) - no_per_page;
                }

                sql = `SELECT lsn_id,
                    admin_lesson.lsn_title,
                    admin_lesson.lsn_vimeo_url,
                    admin_lesson.isn_video_path_hd,
                    admin_lesson.isn_video_path_md,
                    admin_lesson.isn_video_path_sd,
                    admin_lesson.lsn_thumbnail,
                    admin_lesson.lsn_teacher_id,
                    admin_lesson.lsn_student_view,
                    admin_lesson.lsn_description,
                    admin_lesson.lsn_release_date,
                    admin_teacher_info.tch_firstname,
                    admin_teacher_info.tch_lastname,
                    admin_teacher_info.tch_subject_id,
                    admin_lesson_doc.lsd_id,
                    admin_lesson_doc.lsd_title,
                    admin_lesson_doc.lsd_file
                    FROM
                    admin_lesson
                    INNER JOIN admin_teacher_info ON admin_lesson.lsn_teacher_id = admin_teacher_info.tch_id
                    LEFT JOIN admin_lesson_doc ON admin_lesson.lsn_id = admin_lesson_doc.lsd_lesson_id
                    WHERE
                    admin_lesson.lsn_active = 1 AND
                    admin_lesson.lsn_deleted = 0 AND `+where+`
                    ORDER BY lsn_id DESC LIMIT `+limit+` OFFSET `+offset;
                
                
                cnn.query(sql, function(err, rows, fields) {
                    if (err) throw err;
                    if (rows.length > 0) {                    
                        userData = rows;
                        console.log('Result : '+rows.length);
                        var lesson_base_pdf_url = 'https://e-schoolcambodia.com/assets/upload/1/lsd_file/';
                        var i;
                        for (i = 0; i < rows.length; i++) {
                            rows[i]['lesson_pdf_url'] = lesson_base_pdf_url+rows[i]['lsd_file'];                                      
                        }
                        response.json(req, res, {
                            'host': __server_host + 'client/',
                            'data': userData
                        });
                        return;
                    } else {
                        response.json(req, res, {'data': [] } );
                    }
                });
            });
        } else {
            response.error(req, res, 406, "TITLE_REQUIRED");
        }
    } else {
        response.error(req, res, 405, "METHOD_DISALLOW");
    }
};

module.exports.company_info = function(req, res) {
    if (req.method === 'GET') {
        db.connect(function(cnn) {
                var sql = '';
                sql = `SELECT
                    app_config.brand,
                    app_config.company_address,
                    app_config.company_email,
                    app_config.company_logo,
                    app_config.company_name,
                    app_config.company_phone,
                    app_config.company_website,
                    app_config.company_facebook,
                    app_config.company_googleplus,
                    app_config.company_twitter,
                    app_config.company_linkedin,
                    app_config.currency_use,
                    app_config.longitude,
                    app_config.latitude
                    FROM
                    app_config
                    LIMIT 1`;                
                
                cnn.query(sql, function(err, rows, fields) {
                    if (err) throw err;
                    if (rows.length > 0) {
                        userData = rows[0];
                        userData['longitude'] = parseFloat(userData.longitude);
                        userData['latitude'] = parseFloat(userData.latitude);

                        response.json(req, res, {
                            'host': __server_host + 'client/',
                            'data': userData
                        });
                        return;
                    } else {
                        response.error(req,res,401,"RECORD_NOT_FOUND");
                    }
                });
            });
    } else {
        response.error(req, res, 405, "METHOD_DISALLOW");
    }
};

module.exports.user_account = function(req, res) {
    if (req.method === 'POST') {
        var user = req.body.user;
        console.log(user);
        if (user) {
            db.connect(function(cnn) {
                var sql = '';
                sql = `SELECT
                    wb_student_info.std_id,
                    wb_student_info.std_school_name,
                    wb_student_info.std_grade,
                    wb_student_info.std_identify,
                    wb_student_info.std_father_name,
                    wb_student_info.std_father_dob,
                    wb_student_info.std_father_job,
                    wb_student_info.std_father_status,
                    wb_student_info.std_mother_name,
                    wb_student_info.std_mother_dob,
                    wb_student_info.std_mother_job,
                    wb_student_info.std_mother_status,
                    wb_student_info.std_first_name,
                    wb_student_info.std_last_name,
                    wb_student_info.std_native_name,
                    wb_student_info.std_gender_id,
                    wb_student_info.std_dob,
                    wb_student_info.std_nationality,
                    wb_student_info.std_religion,
                    wb_student_info.std_email,
                    wb_student_info.std_place_of_birth,
                    wb_student_info.std_current_address,
                    wb_student_info.std_phone,
                    wb_student_info.std_photo,
                    wb_student_info.std_user_activated,
                    wb_student_info.std_description,
                    wb_student_info.std_student_card,
                    wb_student_info.std_payment_id,
                    wb_student_info.std_register_date
                    FROM
                    wb_student_info
                    WHERE std_id = `+cnn.escape(user);                
                
                cnn.query(sql, function(err, rows, fields) {
                    if (err) throw err;
                    if (rows.length > 0) {
                        userData = rows;

                        response.json(req, res, {
                            'host': __server_host + 'client/',
                            'data': userData
                        });
                        return;
                    } else {
                        response.error(req,res,401,"RECORD_NOT_FOUND");
                    }
                });
            });
        } else {
            response.error(req, res, 406, "USER_REQUIRED");
        }
        
    } else {
        response.error(req, res, 405, "METHOD_DISALLOW");
    }
};

module.exports.user_licence = function(req, res) {
    if (req.method === 'POST') {
        var student_id = req.body.student_id;
        var licence_code = req.body.licence_code;
        console.log(licence_code);
        if(!licence_code){
            response.error(req, res, 406, "LICENCE_CODE_IS_REQUIRED");
        }else if(!student_id){
            response.error(req, res, 405, "STUDENTID_IS_REQUIRED");
        }else{
            db.connect(function(cnn) {
                sql = `SELECT code_id, code_duration, code_term, code_price
                    FROM
                    admin_license_code
                    WHERE code_random = `+cnn.escape(licence_code)+` AND code_use = 0 LIMIT 1`;                
                
                cnn.query(sql, function(err, rows, fields) {
                    if (err) throw err;
                    if (rows.length > 0) {
                        userData = rows[0];
                        var code_id = '';
                        code_id = JSON.stringify(userData.code_id);
                        console.log(code_id);
                        if(code_duration != ''){
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth()+1; //January is 0!
                            var yyyy = today.getFullYear();
                            today = yyyy+'-'+mm+'-'+dd;
                            var code_duration = JSON.stringify(userData.code_duration);
                            var code_term = JSON.stringify(userData.code_term);
                            console.log(code_duration+' '+code_term);
                            sql = `SELECT
                                wb_payment.pay_to_date
                                FROM
                                wb_student_info
                                INNER JOIN wb_payment ON wb_student_info.std_payment_id = wb_payment.pay_id
                                WHERE
                                wb_payment.pay_to_date >= '`+today+`' AND
                                wb_student_info.std_id = `+cnn.escape(student_id);
                            cnn.query(sql, function(err, rows, fields) {
                                if (err) throw err;
                                if (rows.length > 0) {
                                    userData = rows[0];
                                    var pay_to_date = '';
                                    pay_to_date = JSON.stringify(userData.pay_to_date);
                                    pay_to_date = pay_to_date.replace(/^"(.*)"$/, '$1');
                                    console.log(pay_to_date);

                                    if(pay_to_date == ''){
                                        today = today;
                                    }else{
                                        //pay_to_date = pay_to_date = new Date(pay_to_date);
                                        var datetime = require('node-datetime');
                                        var dt = datetime.create(pay_to_date);
                                        today = dt.format('Y-m-d');
                                        console.log(today);
                                        code_term = code_term.replace(/^"(.*)"$/, '$1');
                                        if(code_term == "day"){
                                            // 1 day in the future
                                            dt.offsetInDays(1);
                                        }else if(code_term == "month"){
                                            // 1 month in the future
                                            dt.offsetInDays(31);
                                        }else{
                                            // 1 year in the future
                                            dt.offsetInDays(366);
                                        }
                                        pay_to_date = dt.format('Y-m-d');
                                        //console.log(pay_to_date);
                                    }
                                    var sql = `INSERT INTO wb_payment (pay_student_id, pay_code_id, pay_amount_month, pay_from_date, pay_to_date, payt_amount, pay_type)
                                    VALUES (`+ cnn.escape(student_id) +`, `+ cnn.escape(code_id) +`, 1, `+cnn.escape(today)+`, `+cnn.escape(pay_to_date)+`, 10, 3)`;
                                    cnn.query(sql, function(err, rows, fields) {
                                        if (err) throw err;
                                        if(!err){                                
                                            var sql = `SELECT wb_payment.pay_id FROM wb_payment WHERE wb_payment.pay_student_id = `+ cnn.escape(student_id) +` ORDER BY wb_payment.pay_id DESC LIMIT 1 `;
                                            cnn.query(sql, function(err, rows, fields) {
                                                if (err) throw err;
                                                if (rows.length > 0) {
                                                    userData = rows[0];
                                                    var std_payment_id = userData.pay_id;
                                                    console.log('Pay ID: '+userData.pay_id);
                                                    var sql = `UPDATE wb_student_info SET std_payment_id = `+cnn.escape(std_payment_id)+` WHERE std_id = `+ cnn.escape(student_id);
                                                    cnn.query(sql, function(err, rows, fields) {
                                                        if (err) throw err;
                                                    });

                                                    var sql = `UPDATE admin_license_code SET code_use = 1 WHERE code_id = `+ cnn.escape(code_id);
                                                    cnn.query(sql, function(err, rows, fields) {
                                                        if (err) throw err;
                                                        
                                                    });
                                                    return;
                                                }                       
                                            });
                                            response.json(req, res, {
                                                'data': [{pay_to_date: pay_to_date}],
                                                'status': true
                                            });
                                            return;

                                        }                            
                                    });
                                    return;
                                } else {
                                    response.error(req,res,402,"RECORD_NOT_FOUND_PAY_TO_DATE");
                                }
                            });
                        }else{
                            response.error(req,res,401,"LICENCE_CODE_IS_INCORECT");
                        }
                    } else {
                        response.error(req,res,401,"LICENCE_CODE_IS_INCORECT");
                    }
                });
            });
        }
        
    } else {
        response.error(req, res, 405, "METHOD_DISALLOW");
    }
};